package com.pangram.exercise.pangram

import android.support.v7.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()
        checkPangram()
    }
}

fun checkPangram() {
    val sentences: List<String> = listOf(
        "We promptly judged antique ivory buckles for the next prize.",
        "How razorback jumping frogs can level six piqued gymnasts.",
        "Since we added this pangram section, our visitors have sent in hundreds of their personal favourite pangrams",
        "Blowzy night-frumps vex'd Jack Q.",
        "Just a dummy sentence"
    )

    for ((index: Int, sentence: String) in sentences.withIndex()) {
        println("Sentence $index : $sentence \nPangram :  ${(sentence.isPangram())}")
    }
}

fun String.isPangram(): Boolean {
    if (this.length < 26) return false
    val charArray: CharArray = this.toLowerCase().toCharArray()
    for (ch in 'a'..'z') {
        if (ch !in charArray) return false
    }
    return true
}
